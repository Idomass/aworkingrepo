import sys
import re
from collections import Counter


def print_words(filename):
    """Print words by letter"""
    word_dict = count_words(filename)
    for key in sorted(word_dict):
        print(key, word_dict[key])


def print_top(filename, n):
    """Prints top n words by appearances"""
    word_dict = count_words(filename)
    # Sorts the dictionary by largest value first
    word_dict = {k: v for k, v in sorted(word_dict.items(), key=lambda item: item[1], reverse=True)}
    # prints first n
    for idx, (key, value) in enumerate(word_dict.items()):
        if idx == n:
            break
        print(key, value)



def count_words(filename):
    """count_words(filename (A string))
    Returns a list, with words and number of appearances"""
    with open(filename, 'r') as file:
        word_dict = Counter()
        for line in file:
            res = re.findall("[\w]+[\'][\w]|[\w]+", line.lower()) # Changes to lower, then adds only a-z sequences, and ones with ' in them
            word_dict.update(res)
    return word_dict


def main():
    if len(sys.argv) not in (3, 4):
        print("usage: ./wordcount.py {--topcount | --count {--letter | --appearance}}  file")
        sys.exit(1)

    option = sys.argv[1]
    if option == '--count' and len(sys.argv) == 4:
        option2 = sys.argv[2]
        filename = sys.argv[3]
        if option2 == '--letter':
            print_words(filename)
        elif option2 == '--appearance':
            print_top(filename, -1)
        else:
            print("Bad option: " + option2)
            sys.exit(1)
    elif option == '--topcount' and len(sys.argv) == 3:
        filename = sys.argv[2]
        print_top(filename, 20)
    else:
        print("usage: ./wordcount.py {--topcount | --count {--letter | --appearance}}  file")
        sys.exit(1)    

if __name__ == '__main__':
    main()
